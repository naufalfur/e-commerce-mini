<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transaction_details', function (Blueprint $table) {
            $table->id();
            $table->string('transaction_code');
            $table->string('transaction_number');
            $table->string('product_code');
            $table->integer('price');
            $table->integer('quantity');
            $table->string('unit');
            $table->integer('sub_total');
            $table->string('currency');
            $table->timestamps();

//            $table->unique(['transaction_code', 'transaction_number'], 'transaction_detail_unique');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transaction_details');
    }
}
