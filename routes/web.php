<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ReportController;
use App\Models\TransactionHeader;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    dd($transaction = TransactionHeader::first()->transactionDetails);
});

Route::get('login', [AuthController::class, 'index'])->name('login');
Route::post('login/auth', [AuthController::class, 'login'])->name('login.auth');

Route::group(['middleware' => 'auth'], function() {
    Route::get('dashboard', function() {
        return view('dashboard');
    })->name('dashboard');
    Route::get('logout', [AuthController::class, 'logout'])->name('logout');

    Route::get('products', [ProductController::class, 'list'])->name('products.list');
    Route::get('product/{id}', [ProductController::class, 'detail'])->name('product.detail');

    Route::get('checkout', [ProductController::class, 'checkout'])->name('checkout');

    Route::get('products/carts', [ProductController::class, 'carts'])->name('products.carts');

    Route::post('products/confirm', [ProductController::class, 'confirm'])->name('products.confirm');

    Route::get('report', [ReportController::class, 'index'])->name('report');
    Route::get('report/data', [ReportController::class, 'getData'])->name('report.data');
});
