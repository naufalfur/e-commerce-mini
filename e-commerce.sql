-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               8.0.30 - MySQL Community Server - GPL
-- Server OS:                    Win64
-- HeidiSQL Version:             12.1.0.6537
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Dumping structure for table e-commerce-mini.failed_jobs
CREATE TABLE IF NOT EXISTS `failed_jobs` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce-mini.failed_jobs: ~0 rows (approximately)

-- Dumping structure for table e-commerce-mini.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce-mini.migrations: ~0 rows (approximately)
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(15, '2014_10_12_000000_create_users_table', 1),
	(16, '2014_10_12_100000_create_password_resets_table', 1),
	(17, '2019_08_19_000000_create_failed_jobs_table', 1),
	(18, '2019_12_14_000001_create_personal_access_tokens_table', 1),
	(19, '2024_02_15_052451_create_products_table', 1),
	(20, '2024_02_15_052749_create_transaction_headers_table', 1),
	(21, '2024_02_15_053136_create_transaction_details_table', 1);

-- Dumping structure for table e-commerce-mini.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce-mini.password_resets: ~0 rows (approximately)

-- Dumping structure for table e-commerce-mini.personal_access_tokens
CREATE TABLE IF NOT EXISTS `personal_access_tokens` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce-mini.personal_access_tokens: ~0 rows (approximately)

-- Dumping structure for table e-commerce-mini.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `discount` int NOT NULL,
  `dimension` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `products_code_unique` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce-mini.products: ~3 rows (approximately)
INSERT INTO `products` (`id`, `code`, `name`, `price`, `currency`, `discount`, `dimension`, `unit`, `created_at`, `updated_at`) VALUES
	(1, 'SOKLINPEWANG', 'So Klin Pewangi', 13500, 'IDR', 20, '13 cm x 10 cm', 'PCS', '2024-02-15 07:11:39', NULL),
	(2, 'SOKLINLIQ', 'So Klin Liquid', 18000, 'IDR', 0, '13 cm x 10 cm', 'PCS', '2024-02-15 07:11:39', NULL),
	(3, 'GIVBIRU', 'Giv Biru', 11000, 'IDR', 0, '13 cm x 10 cm', 'PCS', '2024-02-15 07:11:39', NULL);

-- Dumping structure for table e-commerce-mini.transaction_details
CREATE TABLE IF NOT EXISTS `transaction_details` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `transaction_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `price` int NOT NULL,
  `quantity` int NOT NULL,
  `unit` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `sub_total` int NOT NULL,
  `currency` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce-mini.transaction_details: ~5 rows (approximately)
INSERT INTO `transaction_details` (`id`, `transaction_code`, `transaction_number`, `product_code`, `price`, `quantity`, `unit`, `sub_total`, `currency`, `created_at`, `updated_at`) VALUES
	(3, 'TRX', '004', 'SOKLINPEWANG', 13500, 2, 'PCS', 27000, 'IDR', '2024-02-15 02:19:40', '2024-02-15 02:19:40'),
	(4, 'TRX', '004', 'SOKLINLIQ', 18000, 2, 'PCS', 36000, 'IDR', '2024-02-15 02:19:40', '2024-02-15 02:19:40'),
	(5, 'TRX', '005', 'SOKLINPEWANG', 13500, 4, 'PCS', 54000, 'IDR', '2024-02-15 02:21:31', '2024-02-15 02:21:31'),
	(6, 'TRX', '005', 'SOKLINLIQ', 18000, 5, 'PCS', 90000, 'IDR', '2024-02-15 02:21:31', '2024-02-15 02:21:31'),
	(7, 'TRX', '005', 'GIVBIRU', 11000, 2, 'PCS', 22000, 'IDR', '2024-02-15 02:21:31', '2024-02-15 02:21:31'),
	(8, 'TRX', '006', 'SOKLINPEWANG', 13500, 2, 'PCS', 27000, 'IDR', '2024-02-15 03:24:39', '2024-02-15 03:24:39'),
	(9, 'TRX', '006', 'GIVBIRU', 11000, 4, 'PCS', 44000, 'IDR', '2024-02-15 03:24:39', '2024-02-15 03:24:39'),
	(10, 'TRX', '007', 'SOKLINPEWANG', 10800, 2, 'PCS', 21600, 'IDR', '2024-02-15 03:26:34', '2024-02-15 03:26:34'),
	(11, 'TRX', '007', 'SOKLINLIQ', 18000, 2, 'PCS', 36000, 'IDR', '2024-02-15 03:26:34', '2024-02-15 03:26:34');

-- Dumping structure for table e-commerce-mini.transaction_headers
CREATE TABLE IF NOT EXISTS `transaction_headers` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `transaction_code` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `transaction_number` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total` int NOT NULL,
  `transaction_date` date NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `transaction_code_transaction_number` (`transaction_code`,`transaction_number`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce-mini.transaction_headers: ~2 rows (approximately)
INSERT INTO `transaction_headers` (`id`, `transaction_code`, `transaction_number`, `user`, `total`, `transaction_date`, `created_at`, `updated_at`) VALUES
	(4, 'TRX', '004', 'admin', 63000, '2024-02-15', '2024-02-15 02:19:40', '2024-02-15 02:19:40'),
	(5, 'TRX', '005', 'admin', 166000, '2024-02-15', '2024-02-15 02:21:31', '2024-02-15 02:21:31'),
	(6, 'TRX', '006', 'admin', 71000, '2024-02-15', '2024-02-15 03:24:39', '2024-02-15 03:24:39'),
	(7, 'TRX', '007', 'admin', 57600, '2024-02-15', '2024-02-15 03:26:34', '2024-02-15 03:26:34');

-- Dumping structure for table e-commerce-mini.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Dumping data for table e-commerce-mini.users: ~1 rows (approximately)
INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'admin', 'admin@test.com', '2024-02-15 05:56:11', '$2y$10$061QiiEXZX5ZvsSJTgk6kOthTdwLEOdO37cM41AIxFIJgebqGywaq', NULL, '2024-02-15 05:56:16', '2024-02-15 05:56:16');

/*!40103 SET TIME_ZONE=IFNULL(@OLD_TIME_ZONE, 'system') */;
/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
