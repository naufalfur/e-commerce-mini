<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionHeader extends Model
{
    use HasFactory;

    protected $table = 'transaction_headers';
    protected $primaryKey = 'id';

    public function transactionDetails() {
//        foreign key transaction_code and transaction_number
        return $this->hasMany(TransactionDetail::class, 'transaction_code', 'transaction_code')->where('transaction_number', $this->transaction_number);
    }
}
