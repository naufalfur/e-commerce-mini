<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransactionDetail extends Model
{
    use HasFactory;

    protected $table = 'transaction_details';
    protected $primaryKey = 'id';

    public function transactionHeader() {
        return $this->belongsTo(TransactionHeader::class, ['transaction_code','transaction_number'],  ['transaction_code','transaction_number']);
    }
}
