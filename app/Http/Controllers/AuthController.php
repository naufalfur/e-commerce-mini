<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class AuthController extends Controller
{
    //
    public function index() {
        if (Session::has('user')) {
            return redirect('dashboard');
        }

        return view('login.login');
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;

//        dd($username, $password, bcrypt($password), Hash::make($password));
        if (!isset($username) || !isset($password)) {
            Session::flash('error', 'Username and password are required');
            return redirect('login');
        }

        $user = User::where('name', $username)->first();

        if (!isset($user)) {
            Session::flash('error', 'Username not found');
            return redirect('login');
        }

        $attempt = Hash::check($password, $user->password);
        if(!$attempt) {
            Session::flash('error', 'Password is incorrect');
            return redirect('login');
        }

//        Session::put('user', $user);
        Auth::login($user);
        return redirect('dashboard');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('login');
    }
}
