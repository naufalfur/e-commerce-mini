<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\TransactionDetail;
use App\Models\TransactionHeader;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    //
    public function list() {
        $products = Product::all();

        $html_products = '';

//        make a li of producst with 3 section : pseudo background blue for image, product name, price, and price after discount, and a button to add to cart
        foreach ($products as $product) {
//            $html_products .= '<li>';
            $html_products .= '<div class="row mt-3">';
            $html_products .= '<div class="col-md-3">';
            $html_products .= '<div class="pseudo detail-product-item" data-product-id="' . $product->id . '" style="background: aqua; height: 100px; width: 100px;border-radius: 10%;"></div>';
            $html_products .= '</div>';
            $html_products .= '<div class="col-md-5">';
            $html_products .= '<h3>' . $product->name . '</h3>';
            if ($product->discount > 0) {
//
                $html_products .= '<p>' . '<span style="text-decoration: line-through;">' . $product->price . '</span>'. '<br>';
                $html_products .= ($product->price * (1 - $product->discount/100)) . '</p>';
            } else {
                $html_products .= '<p>' . $product->price . '</p>';
            }
            $html_products .= '</div>';
            $html_products .= '<div class="col-md-4">';
            $html_products .= '<button class="btn btn-primary add-to-cart" data-product-id="' . $product->id . '">Buy</button>';
            $html_products .= '</div>';
            $html_products .= '</div>';
//            $html_products .= '</li>';
        }

        return response()->json($html_products);
    }

    public function detail($id) {
        $product = Product::find($id);

        $html = '<div class="row">';
        $html .= '<div class="col-md-6">';
        $html .= '<div class="pseudo" style="background: aqua; height: 200px; width: 200px;border-radius: 10%;"></div>';
        $html .= '</div>';
        $html .= '<div class="col-md-6">';
        $html .= '<h3>' . $product->name . '</h3>';
        if ($product->discount > 0) {
            $html .= '<p>' . '<span style="text-decoration: line-through;">' . $product->price . '</span>'. '<br>';
            $html .= ($product->price * (1 - $product->discount/100)) . '</p>';
        } else {
            $html .= '<p>' . $product->price . '</p>';
        }
        $html .= '<p> Dimension : ' . $product->dimension . '</p>';
        $html .= '<p> Price Unit : ' . $product->unit . '</p>';
        $html .= '</div>';
        $html .= '</div>';

        $html .= '<div class="row mt-3">';
        $html .= '<div class="offset-md-6 col-md-6">';
        $html .= '<button class="btn btn-primary add-to-cart" data-product-id="' . $product->id . '">Buy</button>';
        $html .= '</div>';
        $html .= '</div>';

        return response()->json($html);
    }

    public function checkout() {
        return view('checkout');
    }

    public function carts(Request $request) {
        $carts = $request->get('cart');

        $products = Product::whereIn('id', $carts)->get();

        $html = '';
        $total = 0;

        foreach ($products as $product) {
            if ($product->discount > 0) {
                $product->price = $product->price * (1 - $product->discount/100);
            }

            $html .= '<div class="row">';
            $html .= '<div class="col-md-3">';
            $html .= '<div class="pseudo" style="background: aqua; height: 100px; width: 100px;border-radius: 10%;"></div>';
            $html .= '</div>';
            $html .= '<div class="col-md-9">';
            $html .= '<h3>' . $product->name . '</h3>';
            $html .= '<input type="number" value="1" min="1" class="buy-quantity" data-product-id="' . $product->id . '" style="width: 100px"> ' . $product->unit;
            $html .= '<p class="price-product" data-product-id="' . $product->id . '" data-price="' . $product->price . '">' . $product->price . '</p>';
            $html .= '</div>';
            $html .= '</div>';

            $total += $product->price;
        }

        $html .= '<div class="row mt-3">';
        $html .= '<div class="col-md-12">';
        $html .= '<div class="card">';
        $html .= '<div class="card-body">';
        $html .= '<h5>Total : <span id="total">' . $total . '</span></h5>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';
        $html .= '</div>';

        return response()->json($html);
    }

    public function confirm(Request $request) {
        $carts = $request->post('cart');
        $carts_price = $request->post('cart_price');
        $total = $request->post('total');

        $transaction_id = TransactionHeader::max('id') + 1;

        if ($transaction_id < 10) {
            $transaction_id = '00' . $transaction_id;
        } else if ($transaction_id < 100) {
            $transaction_id = '0' . $transaction_id;
        } else {
            $transaction_id = '' . $transaction_id;
        }

        $transaction = new TransactionHeader();
        $transaction->transaction_code = 'TRX';
        $transaction->transaction_number = $transaction_id;
        $transaction->user = Auth::user()->name;
        $transaction->total = $total;
        $transaction->transaction_date = date('Y-m-d H:i:s');
        $transaction->save();

        foreach ($carts_price as $key => $cart) {
            $cart = (object) $cart;

            $transaction_detail = new TransactionDetail();
            $transaction_detail->transaction_code = $transaction->transaction_code;
            $transaction_detail->transaction_number = $transaction->transaction_number;
            $transaction_detail->product_code = Product::find($cart->id)->code;
            $transaction_detail->price = $cart->price;
            $transaction_detail->quantity = $cart->quantity;
            $transaction_detail->unit = Product::find($cart->id)->unit;
            $transaction_detail->sub_total = $cart->price * $cart->quantity;
            $transaction_detail->currency = Product::find($cart->id)->currency;
            $transaction_detail->save();
        }

        return response()->json('Transaction Success');
    }
}
