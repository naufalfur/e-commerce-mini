<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\TransactionDetail;
use App\Models\TransactionHeader;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;

class ReportController extends Controller
{
    //
    public function index() {
        return view('report');
    }

    public function getData() {
        $transaction = TransactionHeader::all();
//        $data = [];
//        foreach ($transaction as $item) {
//            $data[] = [
//                'id' => $item->id,
//                'transaction' => $item->transaction_code . ' - ' . $item->transaction_number,
//                'user' => $item->user,
//                'total' => $item->total,
//                'transaction_code' => $item->transaction_code,
//                'transaction_number' => $item->transaction_number,
//                'transaction_date' => $item->transaction_date,
//
//            ];
//        }
//
////        datatable data
//        $datatable = datatables::of($transaction);
//        return $datatable->make(true);

        return DataTables::of($transaction)
            ->addColumn('transaction', function($transaction) {
                return $transaction->transaction_code . ' - ' . $transaction->transaction_number;
            })
            ->addColumn('total', function($transaction) {
//                turn to Rp 1.000,00
                return 'Rp ' . number_format($transaction->total, 0, ',', '.');
            })
            ->addColumn('date', function($transaction) {
//                turn to 15 Mei 2021 in Indonesia
                $months = [
                    'January' => 'Januari',
                    'February' => 'Februari',
                    'March' => 'Maret',
                    'April' => 'April',
                    'May' => 'Mei',
                    'June' => 'Juni',
                    'July' => 'Juli',
                    'August' => 'Agustus',
                    'September' => 'September',
                    'October' => 'Oktober',
                    'November' => 'November',
                    'December' => 'Desember',
                ];

                $date = date_create($transaction->transaction_date);
                return date_format($date, 'd ') . $months[date_format($date, 'F')] . date_format($date, ' Y');
            })
            ->addColumn('details', function($transaction)  {
//                state Product Name x Quantity
                $details = '';
                foreach (TransactionDetail::where('transaction_code', $transaction->transaction_code)->where('transaction_number', $transaction->transaction_number)->get() as $detail) {
                    $details .= Product::where('code', $detail->product_code)->first()->name . ' x ' . $detail->quantity . '<br>';
                }
                return $details;
            })
            ->rawColumns(['transaction', 'details'])
            ->make(true);

    }
}
