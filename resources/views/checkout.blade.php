@extends('layout')

@section('title')
    <title>Items Checkout</title>
@endsection
@section('css')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
@endsection

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4>Checkout</h4>
                            {{--                        logout --}}
                            <a href="{{ route('logout') }}" class="btn btn-danger btn-sm float-right">Logout</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="products-list-checkout"></div>

                        <div class="row mt-3">
                            <button class="btn btn-primary btn-xl btn-block" onclick="confirm_cart()">Confirm</button>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


    <script>
        $(document).ready(function () {
            // Function to load products list
            function loadProductsListCheckout() {
                // local storage
                var cart = JSON.parse(localStorage.getItem('cart')) || [];
                if (cart.length > 0) {
                    $.ajax({
                        url: "{{ route('products.carts') }}",
                        type: "GET",
                        data: {
                            cart: cart
                        },
                        success: function (response) {
                            $("#products-list-checkout").html(response);
                        },
                    });
                } else {
                    alert('Cart is empty');
                    window.location.href = "{{ route('dashboard') }}";
                }
            }


            // Function to display loading bar
            function showLoadingBar() {
                $("#loadingBar").show();
                setTimeout(function () {
                    $("#loadingBar").hide();
                }, 2000); // Simulating a delay for demonstration purposes
            }

            $(document).on('keyup', '.buy-quantity', function () {
                var id = $(this).data('product-id');
                var quantity = $(this).val();

                var p_price = $('p[data-product-id="' + id + '"]');
                var price = p_price.data('price');

                $('p[data-product-id="' + id + '"]').text(price * quantity);

                var total = $('#total');
                var new_sum = 0;

                var cart = JSON.parse(localStorage.getItem('cart')) || [];
                cart.forEach(function (item, index) {
                    new_sum += $('p[data-product-id="' + item + '"]').data('price') * $('.buy-quantity[data-product-id="' + item + '"]').val();
                });

                total.text(new_sum);
            });

            // Load products list on page load
            loadProductsListCheckout();

        });


        function confirm_cart() {
            var cart = JSON.parse(localStorage.getItem('cart')) || [];
            var total = $('#total').text();
            if (cart.length > 0) {
                var userConfirm = confirm('Are you sure you want to checkout?');
                if (!userConfirm) {
                    return;
                }

                var cart_price = [];
                cart.forEach(function (item, index) {
                    cart_price.push({
                        id: item,
                        quantity: $('.buy-quantity[data-product-id="' + item + '"]').val(),
                        price: $('p[data-product-id="' + item + '"]').data('price')
                    });
                });

                $.ajax({
                    url: "{{ route('products.confirm') }}",
                    type: "POST",
                    data: {
                        cart: cart,
                        cart_price: cart_price,
                        total: total,
                        _token: "{{ csrf_token() }}"
                    },
                    success: function (response) {
                        alert('Checkout successful');
                        localStorage.removeItem('cart');
                        localStorage.removeItem('cart_price');
                        window.location.href = "{{ route('dashboard') }}";
                    },
                });
            } else {
                alert('Cart is empty');
                window.location.href = "{{ route('dashboard') }}";
            }
        }
    </script>
@endsection
