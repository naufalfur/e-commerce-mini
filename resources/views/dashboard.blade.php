@extends('layout')

@section('title')
    <title>Item Selection Dashboard</title>
@endsection
@section('css')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
@endsection

@section('content')
    <div class="container mt-5">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4>Dashboard</h4>
                            {{--                        logout --}}
                            <a href="{{ route('logout') }}" class="btn btn-danger btn-sm float-right">Logout</a>
                        </div>
                    </div>
                    <div class="card-body">
                        <div id="products-list"></div>

{{--                        checkout button --}}
                        <div class="row mt-3">
                            <button class="btn btn-primary btn-xl btn-block" onclick="checkout()">Checkout</button>
                        </div>

                        {{--                        <div class="form-group">--}}
                        {{--                            <label for="itemSelect">Select Item:</label>--}}
                        {{--                            <select class="form-control" id="itemSelect">--}}
                        {{--                                <option value="shawarma">Shawarma - $15.000</option>--}}
                        {{--                                <option value="friedRice">Fried Rice - $20.000</option>--}}
                        {{--                                <option value="kebab">Kebab - $30.000</option>--}}
                        {{--                            </select>--}}
                        {{--                        </div>--}}

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="product-detail-mode" class="modal" tabindex="-1" role="dialog">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="itemDetails"></div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>


    <script>
        $(document).ready(function () {
            // Function to load products list
            function loadProductsList() {
                // You can replace this with an AJAX call to load products list dynamically
                $.ajax({
                    url: "{{ route('products.list') }}",
                    type: "GET",
                    success: function (response) {
                        $("#products-list").html(response);
                    },
                });
            }

            // Function to display loading bar
            function showLoadingBar() {
                $("#loadingBar").show();
                setTimeout(function () {
                    $("#loadingBar").hide();
                }, 2000); // Simulating a delay for demonstration purposes
            }

            // Function to load item details based on the selected item
            function loadItemDetails(item) {
                // You can replace this with an AJAX call to load item details dynamically
                switch (item) {
                    case "shawarma":
                        $("#itemDetails").html("<p>Shawarma details: $15.000</p>");
                        break;
                    case "friedRice":
                        $("#itemDetails").html("<p>Fried Rice details: $20.000</p>");
                        break;
                    case "kebab":
                        $("#itemDetails").html("<p>Kebab details: $30.000</p>");
                        break;
                    default:
                        $("#itemDetails").html("");
                }
            }

            // function add to cart in local storage
            $(document).on('click', '.add-to-cart', function () {
                var product = $(this).data('product-id');
                var cart = JSON.parse(localStorage.getItem('cart')) || [];

                if (cart.includes(product)) {
                    alert('Product already in cart');
                    // $(this).removeClass('btn-primary add-to-cart').addClass('btn-success added-to-cart').text('Added');
                    $('.add-to-cart').each(function () {
                        if (cart.includes($(this).data('product-id'))) {
                            $(this).removeClass('btn-primary add-to-cart').addClass('btn-success added-to-cart').text('Added');
                        }
                    });
                    return;
                }

                cart.push(product);
                localStorage.setItem('cart', JSON.stringify(cart));
                alert('Product added to cart');

                // change add-to-cart button to added-to-cart
                // $(this).removeClass('btn-primary add-to-cart').addClass('btn-success added-to-cart').text('Added');
                $('.add-to-cart').each(function () {
                    if (cart.includes($(this).data('product-id'))) {
                        $(this).removeClass('btn-primary add-to-cart').addClass('btn-success added-to-cart').text('Added');
                    }
                });
            });

            $(document).on('click', '.added-to-cart', function () {
                var product = $(this).data('product-id');
                var cart = JSON.parse(localStorage.getItem('cart')) || [];
                if (cart.includes(product)) {
                    cart.splice(cart.indexOf(product), 1);
                    localStorage.setItem('cart', JSON.stringify(cart));
                    alert('Product removed from cart');
                } else {
                    alert('Product not in cart');
                }

                // change added-to-cart button to add-to-cart
                // $(this).removeClass('btn-success added-to-cart').addClass('btn-primary add-to-cart').text('Buy');
                $('.added-to-cart').each(function () {
                    if (!cart.includes($(this).data('product-id'))) {
                        $(this).removeClass('btn-success added-to-cart').addClass('btn-primary add-to-cart').text('Buy');
                    }
                });
            });

            $(document).on('click', '.detail-product-item', function () {
                var product = $(this).data('product-id');

                $.ajax({
                    url: "{{ route('product.detail', ':product') }}".replace(':product', product),
                    type: "GET",
                    success: function (response) {
                        $("#itemDetails").html(response);
                    },
                });

                var cart = JSON.parse(localStorage.getItem('cart')) || [];
                if (cart.includes(product)) {
                    $('.add-to-cart').each(function () {
                        if (cart.includes($(this).data('product-id'))) {
                            $(this).removeClass('btn-primary add-to-cart').addClass('btn-success added-to-cart').text('Added');
                        }
                    });
                }
                $('#product-detail-mode').modal('show');
            });

            // Load products list
            loadProductsList();


        });

        // function to checkout
        function checkout() {
            var cart = JSON.parse(localStorage.getItem('cart')) || [];
            if (cart.length > 0) {
                window.location.href = "{{ route('checkout') }}";
            } else {
                alert('Cart is empty');
            }
        }
    </script>
@endsection
