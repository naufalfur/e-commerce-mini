<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    @yield('title')
    @yield('css')
</head>

<body class="bg-light">

@yield('content')

@yield('js')
</body>

</html>
