@extends('layout')

@section('title')
    <title>Report Penjualan</title>
@endsection

@section('css')
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.24/css/jquery.dataTables.css">
@endsection

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <div class="d-flex justify-content-between align-items-center">
                            <h4>Report</h4>
                            <a href="{{ route('logout') }}" class="btn btn-danger btn-sm float-right">Logout</a>
                        </div>
                    </div>
                    <div class="card-body">
{{--                        report --}}
                        <table id="table-report"></table>

                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
    <script src="https://code.jquery.com/jquery-3.6.4.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.5.2/dist/umd/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.24/js/jquery.dataTables.js"></script>


    <script>
        $(document).ready(function () {
            // Function to load products list
            function loadReport() {
                $('#table-report').DataTable({
                    processing: true,
                    serverSide: true,
                    ajax: "{{ route('report.data') }}",
                    columns: [
                        // transaction, user, total, date, details
                        {data: 'transaction', name: 'transaction', title: 'Transaction'},
                        {data: 'user', name: 'user', title: 'User'},
                        {data: 'total', name: 'total', title: 'Total'},
                        {data: 'date', name: 'date', title: 'Date'},
                        {data: 'details', name: 'details', title: 'Details'},
                    ]
                });
            }

            loadReport();


        });


    </script>
@endsection
